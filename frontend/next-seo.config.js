const NextSeo = {
  defaultTitle: "Antares",
  titleTemplate: "%s | Machine Learning",
  description: "Application of machine learning to predict the future",

};

export default NextSeo;
