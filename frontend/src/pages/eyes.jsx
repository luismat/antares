import Eyes from '../components/eyes'
import {Center} from "@chakra-ui/react";


export default function PageEyes() {
    return (
        <>
            <Center>
                <Eyes/>
            </Center>
        </>
    )
}
